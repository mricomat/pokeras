package com.pokeras.pokeras.util;

import codes.derive.foldem.Suit;
import com.pokeras.pokeras.data.CardModel;
import com.pokeras.pokeras.data.HandModel;

import java.util.HashMap;
import java.util.Map;

public abstract class ConstantsUtil {

    public static final double BB = 0.02;
    public static final double SB = 0.01;

    public static final String CALL_3BET = "call_3Bet";
    public static final String FOUR_BET = "4_bet";
    public static final String OR = "or";
    public static final String CC = "cc";
    public static final String THREE_BET = "3_bet";

    // SUIT Types
    public static final String DIAMONDS = "diamonds";
    public static final String CLUBS = "clubs";
    public static final String HEARTS = "hearts";
    public static final String SPADES = "spades";

    // JSON Keys
    public static final String CLASS = "class";
    public static final String YMIN = "ymin";
    public static final String XMIN = "xmin";
    public static final String YMAX = "ymax";
    public static final String XMAX = "xmax";

    // Card Values
    public static final String[] suitTypes = {DIAMONDS, CLUBS, HEARTS, SPADES};
    public static final int[] rankValues = {13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1};

    public static final Map<String, Integer> rankMap = new HashMap<String, Integer>() {{
        put("A", 0);
        put("K", 12);
        put("Q", 1);
        put("J", 10);
        put("10", 9);
        put("9", 8);
        put("8", 7);
        put("7", 6);
        put("6", 5);
        put("5", 4);
        put("4", 3);
        put("3", 2);
        put("2", 1);
    }};

    public static final Map<String, Suit> suitMap = new HashMap<String, Suit>() {{
        put(DIAMONDS, Suit.DIAMONDS);
        put(CLUBS, Suit.CLUBS);
        put(HEARTS, Suit.HEARTS);
        put(SPADES, Suit.SPADES);
    }};

    public static final Map<String, String> suitStringMap = new HashMap<String, String>() {{
        put(DIAMONDS, "d");
        put(CLUBS, "c");
        put(HEARTS, "h");
        put(SPADES, "s");
    }};

    public static final Map<String, String> UTG_OR = new HashMap<String, String>() {{
        // As
        put("AA", FOUR_BET);
        put("AKs", FOUR_BET);
        put("AK", CALL_3BET);
        put("AQs", OR);
        put("AQ", OR);
        put("AJs", OR);
        put("AJ", OR);
        put("A10s", OR);

        // K
        put("KK", FOUR_BET);
        put("KQs", OR);
        put("KQ", OR);
        put("KJs", OR);
        put("KJ", OR);
        put("K10s", OR);

        // Q
        put("QQ", CALL_3BET);
        put("QJs", OR);
        put("Q10s", OR);

        // J
        put("JJ", CALL_3BET);
        put("J10s", OR);

        put("1010", OR);

        put("99", OR);

        put("88", OR);

        put("77", OR);

        put("66", OR);
    }};
    public static final Map<String, String> MP_OR = new HashMap<String, String>() {{
        // As
        put("AA", FOUR_BET);
        put("AKs", FOUR_BET);
        put("AK", CALL_3BET);
        put("AQs", OR);
        put("AQ", OR);
        put("AJs", OR);
        put("AJ", OR);
        put("A10s", OR);
        put("A10", OR);

        // K
        put("KK", FOUR_BET);
        put("KQs", OR);
        put("KQ", OR);
        put("KJs", OR);
        put("KJ", OR);
        put("K10s", OR);
        put("K10", OR);

        // Q
        put("QQ", CALL_3BET);
        put("QJs", OR);
        put("QJ", OR);
        put("Q10s", OR);

        // J
        put("JJ", CALL_3BET);
        put("J10s", OR);

        put("1010", OR);

        put("99", OR);

        put("88", OR);

        put("77", OR);

        put("66", OR);

        put("55", OR);
    }};
    public static final Map<String, String> CO_OR = new HashMap<String, String>() {{
        // As
        put("AA", FOUR_BET);
        put("AKs", FOUR_BET);
        put("AK", FOUR_BET);
        put("AQs", CALL_3BET);
        put("AQ", CALL_3BET);
        put("AJs", CALL_3BET);
        put("AJ", CALL_3BET);
        put("A10s", CALL_3BET);
        put("A10", OR);
        put("A9s", OR);
        put("A8s", OR);
        put("A7s", OR);
        put("A6s", OR);
        put("A5s", OR);
        put("A4s", OR);
        put("A3s", OR);
        put("A2s", OR);

        // K
        put("KK", FOUR_BET);
        put("KQs", CALL_3BET);
        put("KQ", CALL_3BET);
        put("KJs", CALL_3BET);
        put("KJ", OR);
        put("K10s", CALL_3BET);
        put("K10", OR);
        put("K9s", OR);
        put("K8s", OR);

        // Q
        put("QQ", FOUR_BET);
        put("QJs", CALL_3BET);
        put("QJ", OR);
        put("Q10s", OR);
        put("Q10", OR);
        put("Q9s", OR);
        put("Q8s", OR);

        // J
        put("JJ", CALL_3BET);
        put("J10s", OR);
        put("J10", OR);
        put("J9s", OR);
        put("J8s", OR);

        put("1010", CALL_3BET);
        put("109s", OR);
        put("108s", OR);

        put("99", CALL_3BET);
        put("98s", OR);

        put("88", CALL_3BET);
        put("87s", OR);

        put("77", CALL_3BET);
        put("76s", OR);

        put("66", OR);
        put("65s", OR);

        put("55", OR);
        put("54s", OR);

        put("44", OR);

        put("33", OR);
    }};
    public static final Map<String, String> BU_OR = new HashMap<String, String>() {{
        // As
        put("AA", FOUR_BET);
        put("AKs", FOUR_BET);
        put("AK", FOUR_BET);
        put("AQs", CALL_3BET);
        put("AQ", CALL_3BET);
        put("AJs", CALL_3BET);
        put("AJ", CALL_3BET);
        put("A10s", CALL_3BET);
        put("A10", FOUR_BET);
        put("A9s", OR);
        put("A9", OR);
        put("A8s", OR);
        put("A8", OR);
        put("A7s", OR);
        put("A7", OR);
        put("A6s", OR);
        put("A6", OR);
        put("A5s", OR);
        put("A5", OR);
        put("A4s", OR);
        put("A4", OR);
        put("A3s", OR);
        put("A3", OR);
        put("A2s", OR);
        put("A2", OR);

        // K
        put("KK", FOUR_BET);
        put("KQs", CALL_3BET);
        put("KQ", CALL_3BET);
        put("KJs", CALL_3BET);
        put("KJ", FOUR_BET);
        put("K10s", CALL_3BET);
        put("K10", OR);
        put("K9s", OR);
        put("K9", OR);
        put("K8s", OR);
        put("K8", OR);
        put("K7s", OR);
        put("K6s", OR);
        put("K5s", OR);
        put("K4s", OR);
        put("K3s", OR);
        put("K2s", OR);

        // Q
        put("QQ", FOUR_BET);
        put("QJs", CALL_3BET);
        put("QJ", OR);
        put("Q10s", OR);
        put("Q10", OR);
        put("Q9s", OR);
        put("Q9", OR);
        put("Q8s", OR);
        put("Q8", OR);
        put("Q7s", OR);
        put("Q6s", OR);

        // J
        put("JJ", CALL_3BET);
        put("J10s", OR);
        put("J10", OR);
        put("J9s", OR);
        put("J9", OR);
        put("J8s", OR);
        put("J7s", OR);

        put("1010", CALL_3BET);
        put("109s", OR);
        put("109", OR);
        put("108s", OR);
        put("108", OR);
        put("107s", OR);

        put("99", CALL_3BET);
        put("98s", OR);
        put("98", OR);
        put("97s", OR);

        put("88", CALL_3BET);
        put("87s", OR);

        put("77", CALL_3BET);
        put("76s", OR);

        put("66", CALL_3BET);
        put("65s", OR);

        put("55", OR);
        put("54s", OR);

        put("44", OR);

        put("33", OR);
    }};

    public static final Map<String, String> CC_UTG_MP = new HashMap<String, String>() {{
        // As
        put("AA", THREE_BET);
        put("AKs", THREE_BET);
        put("AK", THREE_BET);

        // K
        put("KK", THREE_BET);

        // Q
        put("QQ", THREE_BET);

        // J
        put("JJ", CC);

        put("1010", CC);

        put("99", CC);

        put("88", CC);

        put("77", CC);

        put("66", CC);

        put("55", CC);
    }};
    public static final Map<String, String> CC_CO_BU = new HashMap<String, String>() {{
        // As
        put("AA", THREE_BET);
        put("AKs", THREE_BET);
        put("AK", THREE_BET);
        put("AQs", THREE_BET);
        put("AQ", THREE_BET);
        put("AJs", THREE_BET);
        put("AJ", THREE_BET);
        put("A10s", THREE_BET);

        // K
        put("KK", FOUR_BET);
        put("KQs", THREE_BET);
        put("KQ", THREE_BET);
        put("KJs", THREE_BET);

        // Q
        put("QQ", THREE_BET);

        // J
        put("JJ", THREE_BET);

        put("1010", THREE_BET);

        put("99", THREE_BET);

        put("88", OR);

        put("77", OR);

        put("66", OR);
    }};
    public static final Map<String, String> CC_UTG_MP_VS_BU = new HashMap<String, String>() {{
        // As
        put("AA", THREE_BET);
        put("AKs", THREE_BET);
        put("AK", THREE_BET);
        put("AQs", CC);
        put("AQ", CC);
        put("AJs", CC);
        put("AJ", CC);
        put("A10s", CC);

        // K
        put("KK", THREE_BET);
        put("KQs", CC);
        put("KQ", CC);
        put("KJs", CC);
        put("K10s", CC);

        // Q
        put("QQ", THREE_BET);
        put("QJs", CC);
        put("Q10s", CC);

        // J
        put("JJ", CC);
        put("J10s", CC);

        put("1010", CC);

        put("99", CC);

        put("88", CC);

        put("77", CC);

        put("66", CC);

        put("55", CC);
    }};
    public static final Map<String, String> CC_CO_BU_VS_BB = new HashMap<String, String>() {{
        // As
        put("AA", THREE_BET);
        put("AKs", THREE_BET);
        put("AK", THREE_BET);
        put("AQs", THREE_BET);
        put("AQ", THREE_BET);
        put("AJs", THREE_BET);
        put("AJ", THREE_BET);
        put("A10s", THREE_BET);
        put("A10", CC);
        put("A9s", CC);
        put("A8s", CC);
        put("A7s", CC);
        put("A6s", CC);
        put("A5s", CC);
        put("A4s", CC);
        put("A3s", CC);
        put("A2s", CC);


        // K
        put("KK", THREE_BET);
        put("KQs", THREE_BET);
        put("KQ", THREE_BET);
        put("KJs", THREE_BET);
        put("KJ", CC);
        put("K10s", CC);
        put("K10", CC);
        put("K9s", CC);

        // Q
        put("QQ", THREE_BET);
        put("QJs", CC);
        put("QJ", CC);
        put("Q10s", CC);
        put("Q10", CC);
        put("Q9s", CC);

        // J
        put("JJ", THREE_BET);
        put("J10s", CC);
        put("J10", CC);
        put("J9s", CC);

        put("1010", THREE_BET);
        put("109s", CC);

        put("99", THREE_BET);
        put("98s", CC);

        put("88", CC);
        put("87s", CC);

        put("77", CC);

        put("66", CC);

        put("55", CC);

        put("44", CC);

        put("33", CC);

        put("22", CC);
    }};
}
