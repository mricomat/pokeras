package com.pokeras.pokeras.postflop;

import com.pokeras.pokeras.GameEventHandler;
import com.pokeras.pokeras.data.GameModel;
import com.pokeras.pokeras.data.HandLevel;
import com.pokeras.pokeras.data.HandModel;

public class StrategyPostFlop {

    private GameEventHandler mGameEventListener;
    private GameModel mGameModel;
    private FlopHandRankAnalyzer mFlopHandRankAnalyzer;

    public StrategyPostFlop(GameEventHandler gameEventListener) {
        mGameEventListener = gameEventListener;
    }

    public void strategyPostFlop(GameModel gameModel) {
        mGameModel = gameModel;

        mFlopHandRankAnalyzer = new FlopHandRankAnalyzer(gameModel.getMainPlayer().getCards());

        HandLevel handLevel = setHandLevel();

        // This condition it's only when nobody bets before us.
        // TODO Make the condition when someone bets CB or if he RAISE your CB

        if (isSomeoneRaising()) {
            if (isMyBetBefore()) {
                // This case is when someone Raise or CB when I payed or I made CB.
                someoneRaisingMyBet();
            } else {
                someoneRaisingNoBet();
            }
        } else {
            if (gameModel.getMainPlayer().isAggression()) {
                aggressionVsNoCB(handLevel);
            } else {
                // TODO performACTION CHECK
            }
        }
    }

    private void someoneRaisingNoBet() {
        if (mGameModel.getMainPlayer().isAggression()) {
            // This case is when someone has made CB when I have aggression
            aggressionVsCB();
        } else {
            // This case is when someone has made CB when I don't have aggression
            noAggressionVsCB();
        }
    }

    private void someoneRaisingMyBet() {
        if (mGameModel.getMainPlayer().isAggression()) {
            // This case is when someone has raised my OB when I had aggression
            aggressionVsRaiseMyCB();
        } else {
            // This case is when someone has made CB, I payed and someone later has RAISED.
            noAggressionVsCallAndSomeoneRaise();
        }
    }

    private void noAggressionVsCB() {

    }

    private void aggressionVsCB() {
    }

    private void noAggressionVsCallAndSomeoneRaise() {
    }

    private void aggressionVsRaiseMyCB() {

        // TODO Contemplar caso de cuando hacemos raise y nos vuelve a rubir

        if(isBetterThanDoublePair()) {
            // perform RAISE
        } else {

        }

        if(weHaveDoublePair()) {
            // PerformACTION PAY
        } else {

        }

    }

    private boolean isBetterThanDoublePair() {
        return false;
    }

    private boolean weHaveDoublePair() {
        // Contemplar el caso de que sean dobles parejas desde la mano
        return false;
    }

    private boolean isSomeoneRaising() {
        return false;
    }

    private boolean isMyBetBefore() {
        return false;
    }

    // Case when nobody makes CB or Raise you when you are aggressor.
    private void aggressionVsNoCB(HandLevel handLevel) {
        if (!mGameModel.getMainPlayer().getLastMovement().equals("3BET")) {
            switch (handLevel.getHandType()) {
                case "STRONG":
                    if (isProject()) {
                        // TODO performACTION(OB 2/3 - 3/4 POT)
                    } else {
                        // TODO performACTION(OB 1/2 - 2/3 POT)
                    }
                    break;
                case "SHOWDOWN":
                    // TODO performAction(Check - Call) we have to save that we make it before check to make call later
                    break;
                case "NOTHING_BLUFF":
                    if (isProject()) {
                        // TODO performACTION(OB 2/3 - 3/4 POT)
                    } else {
                        // TODO performACTION(OB 1/2 - 2/3 POT)
                    }
                    break;
                case "NOTHING":
                    break;
            }
        } else {
            // TODO Comprobar la estrategia para entrada a Flop con un 3bet en com.pokeras.pokeras.preflop
        }
    }

    private boolean isProject() {
        return false;
    }

    // Case when we aren't aggressors
    private void postFlopNOAggression(HandLevel handLevel) {

    }

    private HandLevel setHandLevel() {
        HandModel myHand = mGameModel.getMainPlayer().getCards();

        mFlopHandRankAnalyzer.determinateHandRank(mGameModel.getCardList());
        /*
         * Comrobamos si tenemos pareja en mano.
         *  - Si tenemos pareja en mano comprobamos:
         *      - Si tenemos SET - Si tenemos SET comprobamos si hay poker.
         *      - En el caso de que no comprobamos el nivel de la pareja, Si es primera segunda o tercera
         *      - Despues comprobaremos si estas parejas tienen proyecto incluido de color o escalera
         *      Una vez listo dispondremos del nivel
         *
         *   - No tenemos pareja en mano:
         *      - Comprobamos si tenemos una pareja:
         *         En el caso de tenerla comprobamos
         *         - Si hay trio o poker.
         *         - si es primera segunda o tercera.
         *
         *      - En ambos csas de no tener pareja o si, comprobamos si tenemos color o escalera o proyecto.
         *      - En el caso de tener escalera comprobamos si es de color.
         *      - en el caso de tener color comprobamos el nivel del color.
         *       Una vez listo dispondremos del nivel
         *
         */
        return new HandLevel();
    }
}
