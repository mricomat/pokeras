package com.pokeras.pokeras;

import com.pokeras.pokeras.data.GameModel;

public class GameController implements GameEventHandler {

    private GameModel mGameModel;
    private StrategyPreFlopHelper mStrategyPreFlop;

    public GameController(GameModel gameModel) {
        mStrategyPreFlop = new StrategyPreFlopHelper(this);

        onDataReady(gameModel);
    }

    @Override
    public void onDataReady(GameModel gameModel) {
        mGameModel = gameModel;
        switch (gameModel.getGameStage()) {
            case "PRE_FLOP":
                mStrategyPreFlop.strategyPreFlop(gameModel);
                break;
            case "FLOP":
                // STRATEGY FLOP
                break;
            case "TURN":
                // STRATEGY TURN
                break;
            case "RIVER":
                // STRATEGY RIVER
                break;
        }
    }

    @Override
    public void performAction(String action, int rol) {
        System.out.println(action);
    }
}
