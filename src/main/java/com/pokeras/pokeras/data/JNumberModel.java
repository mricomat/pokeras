package com.pokeras.pokeras.data;

public class JNumberModel {

    private int mNumber;
    private int mXmin;
    private int mXmax;
    private int mYmin;
    private int mYmax;

    public int getNumber() {
        return mNumber;
    }

    public void setNumber(int number) {
        mNumber = number;
    }

    public int getXmin() {
        return mXmin;
    }

    public void setXmin(int xmin) {
        mXmin = xmin;
    }

    public int getXmax() {
        return mXmax;
    }

    public void setXmax(int xmax) {
        mXmax = xmax;
    }

    public int getYmin() {
        return mYmin;
    }

    public void setYmin(int ymin) {
        mYmin = ymin;
    }

    public int getYmax() {
        return mYmax;
    }

    public void setYmax(int ymax) {
        mYmax = ymax;
    }
}
