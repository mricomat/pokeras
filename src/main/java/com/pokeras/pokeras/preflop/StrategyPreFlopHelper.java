package com.pokeras.pokeras.preflop;

import com.pokeras.pokeras.GameEventHandler;
import com.pokeras.pokeras.data.GameModel;
import com.pokeras.pokeras.data.HandModel;
import com.pokeras.pokeras.data.PlayerModel;
import com.pokeras.pokeras.util.ConstantsUtil;

import java.util.List;

public class StrategyPreFlopHelper {

    private static final int NO_OR = 0;
    private static final int OR = 1;
    private static final int THREE_BET = 2;
    private static final int FOUR_BET = 3;
    private static final int NO_BET = 0;

    private GameEventHandler mGameEventHandler;
    private GameModel mGameModel;
    private String whoMadeOR;

    public StrategyPreFlopHelper(GameEventHandler gameEventListener) {
        mGameEventHandler = gameEventListener;
    }

    public void strategyPreFlop(GameModel gameModel) {
        mGameModel = gameModel;
        HandModel myHand = gameModel.getMainPlayer().getCards();

        switch (gameModel.getMainPlayer().getTablePosition()) {
            case "UTG":
                UTGStrategyPreFlop(checkTableState(gameModel), myHand);
                break;
            case "MP":
                MPStrategyPreFlop(checkTableState(gameModel), myHand);
                break;
            case "CO":
                COStrategyPreFlop(checkTableState(gameModel), myHand);
                break;
            case "BU":
                BUStrategyPreFlop(checkTableState(gameModel), myHand);
                break;
            case "SB":
                BUStrategyPreFlop(checkTableState(gameModel), myHand);
                break;
            case "BB":
                BBStrategyPreFlop(checkTableState(gameModel), myHand);
                break;
        }
    }

    private void UTGStrategyPreFlop(int option, HandModel hand) {
        switch (option) {
            case NO_OR:
                mGameEventHandler.performAction(ConstantsUtil.UTG_OR.containsKey(hand.getHandToString()) ? "OR" : "FOLD", 0);
                break;
            case OR:
                // This case in our strategy it's not possible because we will Raise or Fold always in UTG.
                // The action will be by default FOLD.
                mGameEventHandler.performAction("FOLD", 0);
                break;
            case THREE_BET:
                if (ConstantsUtil.UTG_OR.containsKey(hand.getHandToString())) {
                    if (ConstantsUtil.UTG_OR.get(hand.getHandToString()).equals(ConstantsUtil.CALL_3BET)) {
                        mGameEventHandler.performAction("CALL_3BET", 0);
                    } else if (ConstantsUtil.UTG_OR.get(hand.getHandToString()).equals(ConstantsUtil.FOUR_BET)) {
                        mGameEventHandler.performAction("4BET", 0);
                    }
                } else {
                    mGameEventHandler.performAction("FOLD", 0);
                }
                break;
            case FOUR_BET:
                mGameEventHandler.performAction("FOLD", 0);
                break;
        }
    }

    private void MPStrategyPreFlop(int option, HandModel hand) {
        switch (option) {
            case NO_OR:
                if (ConstantsUtil.UTG_OR.containsKey(hand.getHandToString())) {
                    mGameEventHandler.performAction("ROL", getHowManyCalls(mGameModel.getPlayerList()));
                } else {
                    mGameEventHandler.performAction(ConstantsUtil.MP_OR.containsKey(hand.getHandToString()) ? "OR" : "FOLD", 0);
                }
                break;
            case OR:
                // In this case we only will see OR from UTG
                performActionVsUTGorMP(hand);
                break;
            case THREE_BET:
                if (ConstantsUtil.MP_OR.containsKey(hand.getHandToString())) {
                    if (ConstantsUtil.MP_OR.get(hand.getHandToString()).equals(ConstantsUtil.CALL_3BET)) {
                        mGameEventHandler.performAction("CALL_3BET", 0);
                    } else if (ConstantsUtil.MP_OR.get(hand.getHandToString()).equals(ConstantsUtil.FOUR_BET)) {
                        mGameEventHandler.performAction("4BET", 0);
                    }
                } else {
                    mGameEventHandler.performAction("FOLD", 0);
                }
                break;
            case FOUR_BET:
                mGameEventHandler.performAction("FOLD", 0);
                break;
        }
    }

    private void COStrategyPreFlop(int option, HandModel hand) {
        switch (option) {
            case NO_OR:
                if (ConstantsUtil.UTG_OR.containsKey(hand.getHandToString())) {
                    mGameEventHandler.performAction("ROL", getHowManyCalls(mGameModel.getPlayerList()));
                } else {
                    mGameEventHandler.performAction(ConstantsUtil.CO_OR.containsKey(hand.getHandToString()) ? "OR" : "FOLD", 0);
                }
                break;
            case OR:
                // In this case we only will see OR from UTG or MP
                performActionVsUTGorMP(hand);
                break;
            case THREE_BET:
                if (ConstantsUtil.CO_OR.containsKey(hand.getHandToString())) {
                    if (ConstantsUtil.CO_OR.get(hand.getHandToString()).equals(ConstantsUtil.CALL_3BET)) {
                        mGameEventHandler.performAction("CALL_3BET", 0);
                    } else if (ConstantsUtil.CO_OR.get(hand.getHandToString()).equals(ConstantsUtil.FOUR_BET)) {
                        mGameEventHandler.performAction("4BET", 0);
                    }
                } else {
                    mGameEventHandler.performAction("FOLD", 0);
                }
                break;
            case FOUR_BET:
                mGameEventHandler.performAction("FOLD", 0);
                break;
        }
    }

    private void BUStrategyPreFlop(int option, HandModel hand) {
        switch (option) {
            case NO_OR:
                performActionBuInNoOR(hand);
                break;
            case OR:
                // In this case we have to see who have done the OR
                if (whoMadeOR.equals("UTG") || whoMadeOR.equals("MP")) {
                    performActionVsUTGorMP(hand);
                } else {
                    performActionVsCOorBU(hand);
                }
                break;
            case THREE_BET:
                performAction3BetInBU(hand);
                break;
            case FOUR_BET:
                mGameEventHandler.performAction("FOLD", 0);
                break;
        }
    }

    private void performActionBuInNoOR(HandModel hand) {
        if (ConstantsUtil.UTG_OR.containsKey(hand.getHandToString())) {
            mGameEventHandler.performAction("ROL", getHowManyCalls(mGameModel.getPlayerList()));
        } else {
            mGameEventHandler.performAction(ConstantsUtil.BU_OR.containsKey(hand.getHandToString()) ? "OR" : "FOLD", 0);
        }
    }

    private void performAction3BetInBU(HandModel hand) {
        if (ConstantsUtil.BU_OR.containsKey(hand.getHandToString())) {
            if (ConstantsUtil.BU_OR.get(hand.getHandToString()).equals(ConstantsUtil.CALL_3BET)) {
                mGameEventHandler.performAction("CALL_3BET", 0);
            } else if (ConstantsUtil.BU_OR.get(hand.getHandToString()).equals(ConstantsUtil.FOUR_BET)) {
                mGameEventHandler.performAction("4BET", 0);
            }
        } else {
            mGameEventHandler.performAction("FOLD", 0);
        }
    }

    private void BBStrategyPreFlop(int option, HandModel hand) {
        switch (option) {
            case NO_OR:
                performActionBuInNoOR(hand);
                break;
            case OR:
                // In this case we have to see who have done the OR
                if (whoMadeOR.equals("UTG") || whoMadeOR.equals("MP")) {
                    performBUActionVsUTGorMP(hand);
                } else {
                    performBUActionVsCOorBUorSB(hand);
                }
                break;
            case THREE_BET:
                performAction3BetInBU(hand);
                break;
            case FOUR_BET:
                mGameEventHandler.performAction("FOLD", 0);
                break;
        }
    }

    private void performActionVsUTGorMP(HandModel hand) {
        if (ConstantsUtil.CC_UTG_MP.containsKey(hand.getHandToString())) {
            if (ConstantsUtil.CC_UTG_MP.get(hand.getHandToString()).equals(ConstantsUtil.CC)) {
                mGameEventHandler.performAction("CC", 0);
            } else if (ConstantsUtil.CC_UTG_MP.get(hand.getHandToString()).equals(ConstantsUtil.THREE_BET)) {
                mGameEventHandler.performAction("3BET", 0);
            }
        } else {
            mGameEventHandler.performAction("FOLD", 0);
        }
    }

    private void performActionVsCOorBU(HandModel hand) {
        if (ConstantsUtil.CC_CO_BU.containsKey(hand.getHandToString())) {
            if (ConstantsUtil.CC_CO_BU.get(hand.getHandToString()).equals(ConstantsUtil.CC)) {
                mGameEventHandler.performAction("CC", 0);
            } else if (ConstantsUtil.CC_CO_BU.get(hand.getHandToString()).equals(ConstantsUtil.THREE_BET)) {
                mGameEventHandler.performAction("3BET", 0);
            }
        } else {
            mGameEventHandler.performAction("FOLD", 0);
        }
    }

    private void performBUActionVsUTGorMP(HandModel hand) {
        if (ConstantsUtil.CC_UTG_MP_VS_BU.containsKey(hand.getHandToString())) {
            if (ConstantsUtil.CC_UTG_MP_VS_BU.get(hand.getHandToString()).equals(ConstantsUtil.CC)) {
                mGameEventHandler.performAction("CC", 0);
            } else if (ConstantsUtil.CC_UTG_MP_VS_BU.get(hand.getHandToString()).equals(ConstantsUtil.THREE_BET)) {
                mGameEventHandler.performAction("3BET", 0);
            }
        } else {
            mGameEventHandler.performAction("FOLD", 0);
        }
    }

    private void performBUActionVsCOorBUorSB(HandModel hand) {
        if (ConstantsUtil.CC_CO_BU_VS_BB.containsKey(hand.getHandToString())) {
            if (ConstantsUtil.CC_CO_BU_VS_BB.get(hand.getHandToString()).equals(ConstantsUtil.CC)) {
                mGameEventHandler.performAction("CC", 0);
            } else if (ConstantsUtil.CC_CO_BU_VS_BB.get(hand.getHandToString()).equals(ConstantsUtil.THREE_BET)) {
                mGameEventHandler.performAction("3BET", 0);
            }
        } else {
            mGameEventHandler.performAction("FOLD", 0);
        }
    }

    // TODO FALTA VER SI LA APUESTA ES DEMASIADO ELEVADA COMO PARA SEGUIRLA
    private int checkTableState(GameModel gameModel) {
        double betCount = NO_BET;
        for (int i = 0; i < gameModel.getPlayerList().size(); i++) {
            double betPlayer = gameModel.getPlayerList().get(i).getBet();
            if (gameModel.getPlayerList().get(i).getBet() > ConstantsUtil.BB) {
                if (betCount != NO_BET && betCount < betPlayer) {
                    return FOUR_BET;
                }
                betCount = betPlayer;
                whoMadeOR = gameModel.getPlayerList().get(i).getTablePosition();
            }
        }
        if (betCount != NO_BET) {
            double mainPlayerBet = gameModel.getMainPlayer().getBet();
            if (mainPlayerBet != NO_BET && mainPlayerBet > ConstantsUtil.BB) {
                return THREE_BET;
            } else {
                return OR;
            }
        } else {
            return NO_OR;
        }
    }

    // Get how many players have done call to the BB in PRE_FLOP. This value it's used in ROL.
    private int getHowManyCalls(List<PlayerModel> playersList) {
        int count = 0;
        for (int i = 0; i < playersList.size(); i++) {
            double betPlayer = playersList.get(i).getBet();
            if (betPlayer == ConstantsUtil.BB) {
                count++;
            }
        }
        return count - 1;
    }
}
