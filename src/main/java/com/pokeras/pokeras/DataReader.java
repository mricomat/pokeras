package com.pokeras.pokeras;

import com.pokeras.pokeras.data.JCardModel;
import com.pokeras.pokeras.data.JDealerModel;
import com.pokeras.pokeras.data.JNumberModel;
import com.pokeras.pokeras.util.ConstantsUtil;
import org.apache.commons.io.FileUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class DataReader {

    // Modify the string where is your dataFile
    private final static String mDirectionFile = "C:\\tensorflow1\\models\\research\\object_detection\\filename_string.json";
    private List<JCardModel> mCardsList = new ArrayList<>();
    private List<JNumberModel> mNumberList = new ArrayList<>();
    private JDealerModel mDealerModel = new JDealerModel();

    public DataReader() {
        readJSONFile();
    }

    private void readJSONFile() {
        File file = new File(mDirectionFile);
        String content;
        try {
            content = FileUtils.readFileToString(file, "utf-8");
            JSONObject jsonObject = new JSONObject(content);

            setCards(jsonObject.getJSONArray("cards"));
            setNumbers(jsonObject.getJSONArray("numbers"));
            setDealer(jsonObject.getJSONArray("dealer"));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void setCards(JSONArray jsonArray) {
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject object = jsonArray.getJSONObject(i);
            JCardModel card = new JCardModel();
            String cardValue = object.get("value").toString();

            for (int k = 0; k < ConstantsUtil.suitTypes.length; k++) {
                if (cardValue.contains(ConstantsUtil.suitTypes[k])) {
                    card.setSuit(ConstantsUtil.suitTypes[k]);
                    break;
                }
            }

            for (int q = 0; q < ConstantsUtil.rankValues.length; q++) {
                if (cardValue.contains(String.valueOf(ConstantsUtil.rankValues[q]))) {
                    card.setRank(ConstantsUtil.rankValues[q]);
                    break;
                }
            }

            card.setYmin((int) object.get(ConstantsUtil.YMIN));
            card.setXmin((int) object.get(ConstantsUtil.XMIN));
            card.setYmax((int) object.get(ConstantsUtil.YMAX));
            card.setXmax((int) object.get(ConstantsUtil.XMAX));

            mCardsList.add(card);
        }
    }

    private void setNumbers(JSONArray jsonArray) {

        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject object = jsonArray.getJSONObject(i);
            JNumberModel numberModel = new JNumberModel();
            numberModel.setNumber((int) object.get("value"));

            numberModel.setYmin((int) object.get(ConstantsUtil.YMIN));
            numberModel.setXmin((int) object.get(ConstantsUtil.XMIN));
            numberModel.setYmax((int) object.get(ConstantsUtil.YMAX));
            numberModel.setXmax((int) object.get(ConstantsUtil.XMAX));

            mNumberList.add(numberModel);
        }
    }

    private void setDealer(JSONArray jsonArray) {
        JSONObject object = jsonArray.getJSONObject(0);
        JDealerModel dealerModel = new JDealerModel();
        dealerModel.setYmin((int) object.get(ConstantsUtil.YMIN));
        dealerModel.setXmin((int) object.get(ConstantsUtil.XMIN));
        dealerModel.setYmax((int) object.get(ConstantsUtil.YMAX));
        dealerModel.setXmax((int) object.get(ConstantsUtil.XMAX));
        mDealerModel = dealerModel;

    }
}
