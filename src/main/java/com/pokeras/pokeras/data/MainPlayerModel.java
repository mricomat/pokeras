package com.pokeras.pokeras.data;

public class MainPlayerModel extends PlayerModel {

    private HandModel mCards;
    private boolean mPosition;
    private boolean mAggression;
    private String mLastMovement;

    public MainPlayerModel(HandModel cards) {
        mCards = cards;
    }

    public HandModel getCards() {
        return mCards;
    }

    public void setCards(HandModel cards) {
        mCards = cards;
    }

    public boolean isPosition() {
        return mPosition;
    }

    public void setPosition(boolean position) {
        mPosition = position;
    }

    public boolean isAggression() {
        return mAggression;
    }

    public void setAggression(boolean aggression) {
        mAggression = aggression;
    }

    public String getLastMovement() {
        return mLastMovement;
    }

    public void setLastMovement(String lastMovement) {
        mLastMovement = lastMovement;
    }
}
