package com.pokeras.pokeras.data;

public class PlayerModel {

    private String mTablePosition;
    private double mBet; // TODO Podria ser un modelo de datos donde almacene si es call, raise o null con la cantidad apostada
    private String[] mBetHistoric;

    public String getTablePosition() {
        return mTablePosition;
    }

    public void setTablePosition(String tablePosition) {
        mTablePosition = tablePosition;
    }

    public double getBet() {
        return mBet;
    }

    public void setBet(double bet) {
        mBet = bet;
    }

    public String[] getBetHistoric() {
        return mBetHistoric;
    }

    public void setBetHistoric(String[] betHistoric) {
        mBetHistoric = betHistoric;
    }
}
