package com.pokeras.pokeras.data;

public class JCardModel {

    private String mSuit;
    private int mRank;
    private int mXmin;
    private int mXmax;
    private int mYmin;
    private int mYmax;

    public JCardModel() {

    }

    public JCardModel(String suit, int rank, int xmin, int xmax, int ymin, int ymax) {
        mSuit = suit;
        mRank = rank;
        mXmin = xmin;
        mXmax = xmax;
        mYmin = ymin;
        mYmax = ymax;
    }

    public String getSuit() {
        return mSuit;
    }

    public void setSuit(String suit) {
        mSuit = suit;
    }

    public int getRank() {
        return mRank;
    }

    public void setRank(int rank) {
        mRank = rank;
    }

    public int getXmin() {
        return mXmin;
    }

    public void setXmin(int xmin) {
        mXmin = xmin;
    }

    public int getXmax() {
        return mXmax;
    }

    public void setXmax(int xmax) {
        mXmax = xmax;
    }

    public int getYmin() {
        return mYmin;
    }

    public void setYmin(int ymin) {
        mYmin = ymin;
    }

    public int getYmax() {
        return mYmax;
    }

    public void setYmax(int ymax) {
        mYmax = ymax;
    }
}
