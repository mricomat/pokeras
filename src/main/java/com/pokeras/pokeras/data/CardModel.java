package com.pokeras.pokeras.data;

public class CardModel {

    private String mSuit;
    private String mRank;
    private String mStage;

    public CardModel(String rank) {
        mRank = rank;
    }

    public CardModel(String rank, String suit) {
        mSuit = suit;
        mRank = rank;
    }

    public String getSuit() {
        return mSuit;
    }

    public void setSuit(String suit) {
        mSuit = suit;
    }

    public String getRank() {
        return mRank;
    }

    public void setRank(String rank) {
        mRank = rank;
    }

    public String getStage() {
        return mStage;
    }

    public void setStage(String stage) {
        mStage = stage;
    }
}
