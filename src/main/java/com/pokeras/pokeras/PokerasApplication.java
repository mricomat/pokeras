package com.pokeras.pokeras;


import com.pokeras.pokeras.data.GameModel;
import com.pokeras.pokeras.data.HandModel;
import com.pokeras.pokeras.data.MainPlayerModel;
import com.pokeras.pokeras.data.PlayerModel;

import java.util.ArrayList;
import java.util.List;

public class PokerasApplication {

    public static void main(String[] args) {

        GameModel gameModel = new GameModel();
        MainPlayerModel mainPlayerModel = new MainPlayerModel(new HandModel("Q", "Q", false));
        mainPlayerModel.setTablePosition("BB");
        gameModel.setMainPlayer(mainPlayerModel);

        gameModel.setCardList(null);
        gameModel.setGameStage("PRE_FLOP");
        List<PlayerModel> playerList = new ArrayList<>();

        PlayerModel playerModel = new PlayerModel();
        playerModel.setBet(0);
        playerModel.setTablePosition("UTG");
        playerList.add(playerModel);

        playerModel = new PlayerModel();
        playerModel.setBet(0.06);
        playerModel.setTablePosition("MP");
        playerList.add(playerModel);

        playerModel = new PlayerModel();
        playerModel.setBet(0);
        playerModel.setTablePosition("BU");
        playerList.add(playerModel);

        playerModel = new PlayerModel();
        playerModel.setBet(0.01);
        playerModel.setTablePosition("SB");
        playerList.add(playerModel);

        playerModel = new PlayerModel();
        playerModel.setBet(0.02);
        playerModel.setTablePosition("BB");
        playerList.add(playerModel);

        gameModel.setPlayerList(playerList);

        GameController gameController = new GameController(gameModel);

        //DataReader dataReader = new DataReader();

    }

}

