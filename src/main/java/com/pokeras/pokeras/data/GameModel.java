package com.pokeras.pokeras.data;

import java.util.ArrayList;
import java.util.List;

public class GameModel {

    private MainPlayerModel mMainPlayer;
    private List<PlayerModel> mPlayerList = new ArrayList<>();
    private List<CardModel> mCardList = new ArrayList<>();
    private String mGameStage;

    public MainPlayerModel getMainPlayer() {
        return mMainPlayer;
    }

    public void setMainPlayer(MainPlayerModel mainPlayer) {
        mMainPlayer = mainPlayer;
    }

    public List<PlayerModel> getPlayerList() {
        return mPlayerList;
    }

    public void setPlayerList(List<PlayerModel> playerList) {
        mPlayerList = playerList;
    }

    public List<CardModel> getCardList() {
        return mCardList;
    }

    public void setCardList(List<CardModel> cardList) {
        mCardList = cardList;
    }

    public String getGameStage() {
        return mGameStage;
    }

    public void setGameStage(String gameStage) {
        mGameStage = gameStage;
    }

    public PlayerModel getPlayerByPosition(String position) {
        for (PlayerModel playerModel: mPlayerList) {
            if (playerModel.getTablePosition().equals(position)) {
                return playerModel;
            }
        }
        return null;
    }
}
