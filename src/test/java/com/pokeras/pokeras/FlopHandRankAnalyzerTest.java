package com.pokeras.pokeras;

import com.pokeras.pokeras.data.CardModel;
import com.pokeras.pokeras.data.HandModel;
import com.pokeras.pokeras.postflop.FlopHandRankAnalyzer;
import org.hamcrest.CoreMatchers;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertThat;

public class FlopHandRankAnalyzerTest {

    FlopHandRankAnalyzer SUT;

    @Before
    public void setup() throws Exception {
        SUT = new FlopHandRankAnalyzer(new HandModel());
    }

    //<unitOfWork>_<stateUnderTest>_<expectedBehaviour>
    @Test
    public void analyzer_As10vsAJQ_showdown() {

        HandModel handModel = new HandModel();
        List<CardModel> mCards = new ArrayList<>();
        CardModel cardModel = new CardModel("A", "clubs");
        mCards.add(cardModel);
        cardModel = new CardModel("J", "diamonds");
        mCards.add(cardModel);
        handModel.setCards(mCards);

        List<CardModel> board = new ArrayList<>();
        CardModel card = new CardModel("A", "diamonds");
        board.add(card);
        card = new CardModel("9", "clubs");
        board.add(card);
        card = new CardModel("2", "spades");
        board.add(card);

        SUT = new FlopHandRankAnalyzer(handModel);
        String result = SUT.determinateHandRank(board);
        assertThat(result, CoreMatchers.is("SHOWDOWN"));
    }
}