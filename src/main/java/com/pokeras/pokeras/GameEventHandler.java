package com.pokeras.pokeras;

import com.pokeras.pokeras.data.GameModel;

public interface GameEventHandler {

    void onDataReady(GameModel gameModel);

    void performAction(String action, int rol);
}
