package com.pokeras.pokeras.postflop;

import codes.derive.foldem.Card;
import codes.derive.foldem.Hand;
import codes.derive.foldem.Poker;
import codes.derive.foldem.board.Board;
import codes.derive.foldem.eval.DefaultEvaluator;
import codes.derive.foldem.eval.HandValue;
import com.pokeras.pokeras.data.CardModel;
import com.pokeras.pokeras.data.HandLevel;
import com.pokeras.pokeras.data.HandModel;
import com.pokeras.pokeras.util.ConstantsUtil;

import java.util.*;

import static codes.derive.foldem.Poker.card;
import static codes.derive.foldem.board.Boards.board;

public class FlopHandRankAnalyzer {

    private HandModel mHandModel;
    private List<CardModel> mTableCards;
    private ArrayList<Integer> mHandRank = new ArrayList<>();
    private ArrayList<Integer> mTableRanks = new ArrayList<>();
    private HandLevel mHandLevel = new HandLevel();

    public FlopHandRankAnalyzer(HandModel handModel) {
        mHandModel = handModel;
    }

    public String determinateHandRank(List<CardModel> tableCards) {
        mTableCards = tableCards;

        decodeRankValues();

        Card card_one = card(mHandRank.get(0), ConstantsUtil.suitMap.get(mHandModel.getCards().get(0).getSuit()));
        Card card_two = card(mHandRank.get(1), ConstantsUtil.suitMap.get(mHandModel.getCards().get(1).getSuit()));

        Hand hand = Poker.hand(card_one, card_two);

        /* Create a board. */
        Board board = board(boardFormatter());

        DefaultEvaluator evaluator = new DefaultEvaluator();

        HandValue handValue = evaluator.value(hand, board);

        // Check Projects
        checkHandProject();
        checkTableProject();

        switch (handValue.rank()) {
            case 1:
                mHandLevel.setHandType("NOTHING");
                break;
            case 2:
                // PAIR
                if (!mTableRanks.contains(card_one.getValue()) && !mTableRanks.contains(card_two.getValue())) {
                    // No tenemos nada pero es Carta Doblada lo qual es BLUFF si somos agresores
                    // NOTHING_BLUFF
                    mHandLevel.setHandType("NOTHING");
                } else if (mTableRanks.contains(card_one.getValue())) {
                    mHandLevel.setHandType(getTypeOfPair(card_one.getValue(), card_two.getValue()));
                } else {
                    mHandLevel.setHandType(getTypeOfPair(card_two.getValue(), card_one.getValue()));
                }
                break;
            case 3:
                // DOUBLE PAIR
                if (mTableRanks.contains(card_one.getValue()) && mTableRanks.contains(card_two.getValue())) {
                    mHandLevel.setHandType("STRONG");
                } else if (mTableRanks.contains(card_one.getValue())) {
                    // Si tiene de kicker J o más será fuerte si no, ShowDown.
                    checkHighKicker(card_two);
                } else if (mTableRanks.contains(card_two.getValue())) {
                    checkHighKicker(card_one);
                } else {
                    mHandLevel.setHandType("NOTHING");
                }
                break;
            case 4:
                // SET
                if (mTableRanks.contains(card_one.getValue()) && mTableRanks.contains(card_two.getValue())
                        || mTableRanks.contains(card_one.getValue()) || mTableRanks.contains(card_two.getValue())) {
                    // We have pair in hand and SET in total
                    mHandLevel.setHandType("STRONG");
                }
                break;
            case 5:
                // STRAIGHT
                mHandLevel.setHandType("STRONG");
                break;
            case 6:
                // FLUSH
                mHandLevel.setHandType("STRONG");
                break;
            case 7:
                // FULL HOUSE
                mHandLevel.setHandType("STRONG");
                break;
            case 8:
                mHandLevel.setHandType("STRONG");
                break;
            case 9:
                mHandLevel.setHandType("STRONG");
                break;
        }
        return mHandLevel.getHandType();
    }

    private void checkHighKicker(Card card) {
        if (card.getValue() == 10 || card.getValue() == 11 ||
                card.getValue() == 12 || card.getValue() == 0) {
            mHandLevel.setHandType("STRONG");
        } else {
            mHandLevel.setHandType("SHOWDOWN");
        }
    }

    private String getTypeOfPair(int rankPairCard, int rankKicker) {
        int highValue = 0;
        for (int value : mTableRanks) {
            if (highValue < value) {
                highValue = value;
            }
        }
        if (highValue <= rankPairCard) {
            if (rankKicker == 10 || rankKicker == 11 || rankKicker == 12 || rankKicker == 0) {
                return "STRONG";
            } else {
                return "SHOWDOWN";
            }
        } else {
            return "NOTHING";
        }
    }

    private String boardFormatter() {
        StringBuilder boardString = new StringBuilder();
        for (CardModel tableCard : mTableCards) {
            boardString.append(tableCard.getRank()).append(ConstantsUtil.suitStringMap.get(tableCard.getSuit()));
        }
        return boardString.toString();
    }

    private void decodeRankValues() {
        mHandRank.add(ConstantsUtil.rankMap.get(mHandModel.getCards().get(0).getRank()));
        mHandRank.add(ConstantsUtil.rankMap.get(mHandModel.getCards().get(1).getRank()));

        for (int i = 0; i < mTableCards.size(); i++) {
            mTableRanks.add(ConstantsUtil.rankMap.get(mTableCards.get(0).getRank()));
        }
    }

    private void checkTableProject() {
        mHandLevel.setTableProject(false);
        if (isFlushTableProject()) {
            mHandLevel.setTableProject(true);
            mHandLevel.setCounterTableProjects(mHandLevel.getCounterHandProjects() + 1);
        }
        if (isStraightTableProject()) {
            mHandLevel.setTableProject(true);
            mHandLevel.setCounterTableProjects(mHandLevel.getCounterHandProjects() + 1);
        }
    }

    private void checkHandProject() {
        mHandLevel.setHandProject(false);
        if (isFlushHandProject()) {
            mHandLevel.setHandProject(true);
            mHandLevel.setCounterHandProjects(mHandLevel.getCounterHandProjects() + 1);
        }
        if (isStraightHandProject()) {
            mHandLevel.setHandProject(true);
            mHandLevel.setCounterHandProjects(mHandLevel.getCounterHandProjects() + 1);
        }
    }

    private boolean isFlushTableProject() {
        String suit = mTableCards.get(0).getSuit();
        for (int i = 1; i < mTableCards.size(); i++) {
            if (!suit.equals(mTableCards.get(i).getSuit())) {
                return false;
            }
        }
        return true;
    }

    private boolean isStraightTableProject() {
        List<Integer> ranks = mTableRanks;
        Collections.sort(ranks);
        for (int i = 1; i < ranks.size(); i++) {
            if (!ranks.get(i -1).equals(ranks.get(i) - 1)) {
                return false;
            }
        }
        return true;
    }

    private boolean isFlushHandProject() {
        Map<String, Integer> suitReps = new HashMap<>();
        List<CardModel> mCards = mTableCards;
        mCards.addAll(mHandModel.getCards());
        for (CardModel tableCard : mCards) {
            suitReps.put(tableCard.getSuit(), suitReps.get(tableCard.getSuit()) + 1);
            if (suitReps.containsValue(4)) {
                return true;
            }
        }
        return false;
    }

    private boolean isStraightHandProject() {
        List<Integer> ranks = mTableRanks;
        ranks.addAll(mHandRank);
        Collections.sort(ranks);

        int counter = 0;

        for (int i = 1; i < ranks.size(); i++) {
            if (ranks.get(i - 1).equals(ranks.get(i) - 1) || ranks.get(i - 1).equals(ranks.get(i) - 2)) {
                counter++;
            }
            if (counter > 2) {
                return true;
            }
        }
        return false;
    }
}
