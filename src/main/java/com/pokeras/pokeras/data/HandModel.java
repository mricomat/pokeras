package com.pokeras.pokeras.data;

import java.util.ArrayList;
import java.util.List;

public class HandModel {

    private List<CardModel> mCards = new ArrayList<>();
    private boolean mIsSuited;

    public HandModel() {

    }

    public HandModel(String value_one, String value_two, boolean isSuited) {
        mCards.add(new CardModel(value_one));
        mCards.add(new CardModel(value_two));
        mIsSuited = isSuited;
    }

    public HandModel(List<CardModel> cards, boolean isSuited, boolean isPairs) {
        mCards = cards;
        mIsSuited = isSuited;
    }

    public List<CardModel> getCards() {
        return mCards;
    }

    public void setCards(List<CardModel> cards) {
        mCards = cards;
    }

    public boolean isSuited() {
        return mIsSuited;
    }

    public void setSuited(boolean suited) {
        mIsSuited = suited;
    }

    public String getHandToString() {
        String hand = mCards.get(0).getRank() + mCards.get(1).getRank();
        if (isSuited()) {
            hand = hand + "s";
        }
        return hand;
    }

    public boolean isPair() {
        return mCards.get(0).getRank().equals(mCards.get(1).getRank());
    }
}
