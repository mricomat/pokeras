package com.pokeras.pokeras.data;

public class HandLevel {

    private String handType;
    private boolean isHandProject;
    private int counterHandProjects;
    private boolean isTableProject;
    private int counterTableProjects;

    public String getHandType() {
        return handType;
    }

    public void setHandType(String handType) {
        this.handType = handType;
    }

    public boolean isHandProject() {
        return isHandProject;
    }

    public void setHandProject(boolean handProject) {
        isHandProject = handProject;
    }

    public boolean isTableProject() {
        return isTableProject;
    }

    public void setTableProject(boolean tableProject) {
        isTableProject = tableProject;
    }

    public int getCounterHandProjects() {
        return counterHandProjects;
    }

    public void setCounterHandProjects(int counterHandProjects) {
        this.counterHandProjects = counterHandProjects;
    }

    public int getCounterTableProjects() {
        return counterTableProjects;
    }

    public void setCounterTableProjects(int counterTableProjects) {
        this.counterTableProjects = counterTableProjects;
    }
}
